![Arch Logo](https://www.archlinux.org/static/logos/archlinux-logo-dark-90dpi.ebdee92a15b3.png "Arch Linux")

# Download Arch Linux

1.  Download ISO from [official site](https://www.archlinux.org/download/)
2.  Verify content with MD5 hash with `md5sum archlinux.iso`
3.  Verify PGP signature with `gpg --verify archlinux.iso.sig`

# Create Live USB

1.  Write ISO to USB `dd bs=4M if=archlinux.iso of=/dev/sdx status=progress oflag=sync`

# Install Arch Linux

1.  Connect to wifi
2.  Download script
3.  Run script

```sh
wifi-menu
curl -O https://gitlab.com/clintmoyer/setup-os/raw/master/arch/setup.sh
sh setup.sh
```

