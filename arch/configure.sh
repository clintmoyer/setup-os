# Copyright 2019 Clint Moyer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set_timezone() {
    local timezone

    echo "About to list timezones, write yours down to enter in a moment."
    echo
    printf "Press <Enter> to print list."
    read
    timedatectl list-timezones
    echo
    printf "Enter your timezone (example: US/Mountain): "
    read timezone

    ln -sf "/usr/share/zoneinfo/$timezone" /etc/localtime
    hwclock --systohc
    locale-gen
    localectl set-locale en_US.UTF-8
}

set_hostname() {
    local hostname

    printf "Enter a hostname: "
    read hostname
    echo "$hostname" > /etc/hostname
    cat << HERE > /etc/hosts
127.0.0.1   localhost
::1         localhost
127.0.1.1   ${hostname}.localdomain   $hostname
HERE
}

configure_users() {
    local user
    local password

    printf "Enter username: "
    read user
    useradd --create-home "$user"

    stty -echo
    printf "Enter Password: "
    read password
    stty echo
    echo
    echo "root:${password}" | chpasswd
    echo "${user}:${password}" | chpasswd

    sed -i '/%sudo/s/^# //' /etc/sudoers
    usermod -aG sudo "$user"
    usermod -aG docker "$user"
}

install_packages() {
    pacman -S --noconfirm \
        aws-cli \
        beets \
        bluez \
        bluez-utils \
        cowsay \
        dialog \
        docker \
        feh \
        firefox \
        flake8 \
        gimp \
        git \
        gwenview \
        hspell \
        htop \
        kate \
        kcalc \
        kdenlive \
        keepass \
        kmymoney \
        konsole \
        krita \
        mercurial \
        mpv \
        ntop \
        openssh \
        plasma \
        pulseaudio \
        rtorrent \
        thunderbird \
        tree \
        vagrant \
        valgrind \
        vim \
        virt-manager \
        wget \
        wpa_supplicant \
        xorg \
        youtube-dl
}

install_aur_mgr() {
    newgrp sudo
    git clone https://aur.archlinux.org/trizen.git
    makepkg -si BUILDDIR=trizen
    rm -rf trizen
}

install_aur_packages() {
    newgrp sudo
    trizen -S --noedit --noconfirm \
        amarok
}

enable_services() {
    ln -s /usr/lib/systemd/system/sddm.service /etc/systemd/system/display-manager.service
    ln -s /usr/lib/systemd/system/iptables.service /etc/systemd/system/multi-user.target.wants/iptables.service
    ln -s /usr/lib/systemd/system/ip6tables.service /etc/systemd/system/multi-user.target.wants/ip6tables.service
}

setup_bootloader() {
    bootctl --path=/boot install
}

setup_firewall() {
    iptables-restore < /etc/iptables/simple_firewall.rules
    iptables-save > /etc/iptables/iptables.rules
    iptables-save > /etc/iptables/ip6tables.rules
}

main() {
    set_timezone
    set_hostname
    configure_users
    install_official_packages
    #install_aur_mgr
    #install_aur_packages
    enable_services
    setup_bootloader
    setup_firewall
}

set -eo pipefail
main

