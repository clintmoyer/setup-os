# Copyright 2019 Clint Moyer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set_network_time() {
    timedatectl set-ntp true
}

format_disk() {
    local device
    local partition_efi
    local partition_main
    local check_format

    lsblk

    echo
    printf "Enter device (example: /dev/sda): "
    read device
    echo
    printf "Are you sure you want to format %s? [y/N]: " "$device"
    read check_format
    if [ "$check_format" != "y" ] && [ "$check_format" != "Y" ]; then
        echo "Aborting" >&2
        exit 1
    fi

    partition_efi="${device}1"
    partition_main="${device}2"


    parted --script "$device" \
        mktable gpt \
        mkpart primary fat32 1MiB 261MiB \
        mkpart primary ext4 261MiB 100% \
        set 1 esp on

    mkfs.fat -F32 "$partition_efi"

    mkfs.ext4 -F "$partition_main"

    mount "$partition_main" /mnt
    mkdir /mnt/boot
    mount "$partition_efi" /mnt/boot
}

prep_bootstrap() {
    curl -o /mnt/configure.sh https://gitlab.com/clintmoyer/setup-os/raw/master/arch/configure.sh
    curl -o /mnt/post_boot.sh https://gitlab.com/clintmoyer/setup-os/raw/master/arch/post_boot.sh
}

bootstrap() {
    pacstrap /mnt base base-devel
    genfstab -U /mnt >> /mnt/etc/fstab
    arch-chroot /mnt sh configure.sh
}

configure_bootloader() {
    local partition_main_uuid

    partition_main_uuid="$(findmnt -n -o UUID -T /mnt)"

    cp /mnt/usr/share/systemd/bootctl/loader.conf /mnt/boot/loader/loader.conf
    echo "timeout 4" >> /mnt/boot/loader/loader.conf
    echo "editor no" >> /mnt/boot/loader/loader.conf

    head -n -1 /mnt/usr/share/systemd/bootctl/arch.conf > /mnt/boot/loader/entries/arch.conf
    echo "options root=UUID=${partition_main_uuid} rootfstype=ext4 add_efi_memmap" >> /mnt/boot/loader/entries/arch.conf
}

reboot_system() {
    echo "Need to reboot now"
    umount -R /mnt
    systemctl reboot
}

main() {
    set_network_time
    format_disk
    prep_bootstrap
    bootstrap
    configure_bootloader
    reboot_system
}

set -eo pipefail
main

